package App;

public class Prueba {

	public static void Metodo(int num, int base) {
        System.out.println(convertir(num,base));
    }
     
    private static String convertir(int n, int b){
        String salida = "";
        int div=n, mod=0;
        
        while(n>=b){
            div = n / b;
            mod = n % b;  
            salida = charAt(mod) + salida;
            n=div;
        }

        if(div > 0)
            salida = charAt(div) + salida;
        return salida;
    
    }
 
    private static String charAt(int pos){
        return String.valueOf("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(pos));
    }
	
}