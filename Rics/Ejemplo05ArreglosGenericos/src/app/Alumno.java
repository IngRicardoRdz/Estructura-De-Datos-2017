package app;

public class Alumno extends Persona{
	
	private String matricula;
	private String carrera;
	private String grado;
	
	public Alumno(){ //Constructor
		super();
		this.matricula = "123456";
		this.grado = "Primero";
		this.carrera = "No asignado";
	}

	public Alumno(String nombre, int edad, boolean genero) { //Constructor SuperClass
		super(nombre, edad, genero);
		// TODO Auto-generated constructor stub
	}

	public Alumno(String matricula, String carrera, String grado) { //Constructor
		super();
		this.matricula = matricula;
		this.carrera = carrera;
		this.grado = grado;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}
	
}