package queve;

import java.lang.reflect.Array;
import java.util.Iterator;

public class Queve<T extends Comparable<T>> implements iQueve<T>, Iterable<T> {
	
	private T[] 		 data 	= null;
	private Class<T> type 	= null;
	private int 		 frount = -1; //SALIDA
	private int 		 back = 0; //GUARDAR
	private int		 count = 0;
	
	public Queve(Class <T> type) {
		data = (T[])Array.newInstance(type, 10);
		this.type = type;
	}

	public Queve(Class <T> type, int size) {
		data = (T[])Array.newInstance(type, size);
		this.type = type;
	}

/*---------------------------------------------------------*/
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void enQueve(T value) throws QueveFullException {
		
		if (isFull()) throw new QueveFullException("La cola esta llena");
		data[back++ % data.length] = value;
		count++;
		
	}

	@Override
	public T deQueve() throws QueveEmptyException {
		
		if(isEmpty()) throw new QueveEmptyException("La cola esta vacia");
		count--;
		return data[++frount % data.length];
	
	}

	@Override
	public boolean isEmpty() {

		return (count == 0);
	
	}

	@Override
	public boolean isFull() {
		
		return (count == data.length);
	
	}

	@Override
	public T front() throws QueveEmptyException {
		
		if(isEmpty()) throw new QueveEmptyException("La cola esta vacia");
		return data[(frount+1) % data.length];
		
	}

	@Override
	public T search(T value) throws QueveEmptyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean frontOf(T value, int priority) throws QueveFullException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void size() {
		// TODO Auto-generated method stub
		
	}
	
}
