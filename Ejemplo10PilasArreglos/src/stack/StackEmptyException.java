package stack;

@SuppressWarnings("serial")
public class StackEmptyException extends Exception {
	
	public StackEmptyException(){
		// TODO Auto-generated constructor stub
	}
	
	public StackEmptyException(String causa){
		super (causa);
	}
	
	public StackEmptyException(Throwable causa){
		super (causa);
	}
	
}
