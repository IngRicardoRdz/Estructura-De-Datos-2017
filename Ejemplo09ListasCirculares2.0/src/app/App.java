package app;

import circularlist.CircularList;

public class App {

	public static void main(String[] args) {
		CircularList<String> names = new CircularList<>();
		
		names.addLast("Ricardo");
		names.addLast("Alonso");
		names.addFirst("Anna");
		names.addFirst("Yarely");
		names.addFirst("Jacqueline");
		names.addFirst("Laura");
		names.addFirst("Gabriela");
		names.addFirst("Angelica");
		
		names.addBefore("Angelica", "Roxana");
		names.addAfter("Angelica", "Roxx");
		names.replace("Anna", "Rosio");
		names.printer();
//		names.eliminar();
		System.out.println("");
		System.out.println("Primero: " + names.getFirstt());
		System.out.println("Ultimo: " + names.getLastt());
		System.out.println("");
		System.out.println("Encontrado: " + names.Search("Gabriela"));
		System.out.println("");
		names.remove("Laura");
		names.printer();
		System.out.println("");
		System.out.println("Esta basia: " + names.isEmpty());
		System.out.println("");
//		names.eliminar();
		System.out.println("Esta basia: " + names.isEmpty());
		System.out.println("");
		names.removefirst();
		names.removelast();
		names.removeafter("Gabriela");
		names.removebefore("Gabriela");
		names.printer();
		System.out.println("");
		System.out.println("IndexOf: " + names.indexof("Angelica"));
		System.out.println("Size: " + names.size());
	}

}
