package app;

public class GnomeSort  {

	public static <T extends Comparable<T>> T[] gnomeSort(T[] array) {
	      for ( int index = 1; index < array.length; ) {
	         if (array[index - 1].compareTo(array[index]) <= 0) {
	            ++index;
	         } else {
	            T tempVal = (T) array[index];
	            array[index] = array[index - 1];
	            array[index - 1] =  tempVal;
	            --index;
	            if ( index == 0 ) {
	               index = 1;
	            }
	         }
	      }
	      return array;
   }
}
// http://algorithmmx.blogspot.mx/2011/11/algoritmo-de-busqueda-binaria.html