package app;

import java.util.Scanner;

public class Main {
	static String salida = "";
    static int div = 0;
	static int mod=0, cont = 0;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); 
		System.out.print("Introdusca un numero: "); int num = sc.nextInt();
		System.out.print("Introdusca la base: "); int base = sc.nextInt();
		div = num;
		System.out.println("Su convercion es "+convertir(num,base));
		sc.close();
	}
	private static String convertir(int n, int b) { 
		if(n < b) {
			if(div > 0)
				salida = String.valueOf("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(div)) + salida;
			return salida;
		}else {
			div = n / b;
            mod = n % b;
            salida = String.valueOf("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(mod)) + salida;
            n=div;
            return convertir(n, b);
		}
	}
}