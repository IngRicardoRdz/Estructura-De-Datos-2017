package app;

import stack.Stack;

public class App {

	public static void main(String[] args) {
		Stack <String> names = new Stack<String>(5);
		
		try {
			names.push("Mari");
			names.push("Angelica");
			names.push("Angy");
			names.push("Susana");
			names.push("Laura");
			
//			names.clear();
			System.out.println("Metodo POP: " + names.pop());
			
			System.out.println("");
			
			System.out.println("Encontrado: " + names.search("Angy"));
			names.push("Karina");
//			names.push("Jacqueline");
			System.out.println("Ultimo ingresado: " + names.peek());
			
			System.out.println("");
			
			for (String n : names) {
				System.out.println(n);
			}
			
			System.out.println("");
			
			System.out.println("Datos dentro: " + names.size());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}