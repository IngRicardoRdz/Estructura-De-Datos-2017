package app;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[] m = new String[12]; 
		m[0] ="Enero";
		m[1] ="Febrero";
		m[2] ="Marzo";
		m[3] ="Abril";
		m[4] ="Mayo";
		m[5] ="Junio";
		m[6] ="Julio";
		m[7] ="Agosto";
		m[8] ="Septiembre";
		m[9] ="Octubre";
		m[10] ="Noviembre";
		m[11] ="Diciembre";
		
		Sales[][] venta = new Sales [12][];
		venta [0]     = new Sales[31];
		venta [1]     = new Sales[28];
		venta [2]     = new Sales[31];
		venta [3]     = new Sales[30];
		venta [4]     = new Sales[31];
		venta [5]     = new Sales[30];
		venta [6]     = new Sales[31];
		venta [7]     = new Sales[31];
		venta [8]     = new Sales[30];
		venta [9]     = new Sales[31];
		venta [10]    = new Sales[30];
		venta [11]    = new Sales[31];
		int mes =1;
		
		for (int i = 0; i < venta.length; i++) {
			System.out.println((m[i]));
			for (int j = 0; j < venta[i].length; j++) {
				venta[i][j] = new Sales((m[i]),(j+1),(float)(Math.random()*(1000) + 1));
			
				System.out.print(" ["+venta[i][j]+"]");
				System.out.print("\t");
			}
			System.out.println("--------------------------------------------------------------");
			mes=mes+1;
		}
	
		System.out.println("selecciona el mes");
		System.out.println("");
		System.out.println("");
		Sales[] D31 = new Sales[31];
		 D31 = gnomeSort(venta[0]); // SE MANDA A quicksort Y SE GUARDA EN UN ARREGLO
		 
		 
		 
		 
		 for (int i = 0; i < D31.length; i++) {
			System.out.print(D31[i]); // SE IMPRIME EL ARREGLO "ORDENADO"
			System.out.print("\t");
		}
		
		 Scanner entradaEscaner = new Scanner (System.in);
		 double ventass = entradaEscaner.nextFloat();
		 busquedaBinaria(D31, ventass, 0);
	}
	
	@SuppressWarnings("unused")
	private static <T extends Comparable<T>> T[] gnomeSort(T[] array) {
	      for ( int index = 1; index < array.length; ) {
	         if (array[index - 1].compareTo(array[index]) <= 0) {
	            ++index;
	         } else {
	            T tempVal = (T) array[index];
	            array[index] = array[index - 1];
	            array[index - 1] =  tempVal;
	            --index;
	            if ( index == 0 ) {
	               index = 1;
	            }
	         }
	      }
	      return array;
}
	
	public static <T extends Comparable<T>>  Sales[] quicksort(Sales[] b,int low, int high) {
        int i = low,  j = high;
        Sales pivot =  b[low + (high-low)/2];
        while (i <= j) {
          while (b[i].compareTo(pivot) == 0) {
               i++;
           }
            while (b[j].compareTo(pivot) == 0) {
               j--;
            }
           if (i <= j) {
               //exchange
           	 Sales temp = (Sales) b[i];
           	 b[i]=b[j];
           	 b[j]=temp;
               i++;
               j--;
           }
       }
        if (low < j)
            quicksort(b,low, j);
        if (i < high)
            quicksort(b,i, high);
      return b;   
    }
	
	public static void busquedaBinaria( Sales [] venta2, double vta, int gg){
		  int n = venta2.length;
		  int centro,inf=0,sup=n-1;
		   while(inf<=sup){
		     centro=(inf+sup)/2;
		     if(venta2[centro].getVentas() == vta)
		     {
		    	 System.out.println("La venta fue encontrada: "+venta2[centro]);
		    	 return;
		    	 
		     }else if(venta2[centro].getVentas() > vta ){
		        sup=centro-1;
		     }
		     else {
		       inf=centro+1;
		     }
		   }
		   System.out.println("No se encontro la venta");
		   return;
		 }
}
	

	