package app;

public class alumno extends Persona {

	private String matricula;
	private String carrera;
	private String grado;

	public alumno() {
		super();
		this.matricula = "111111111";
		this.grado = "Primero";
		this.carrera = "No asignado";
	}

	public alumno(String nombre, int edad, boolean genero) {
		super(nombre, edad, genero);
	}

	public alumno(String matricula, String carrera, String grado) {
		super();
		this.matricula = matricula;
		this.carrera = carrera;
		this.grado = grado;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}
}
